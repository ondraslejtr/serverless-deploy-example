# Serverless framework Flask app example

Example for difference between deploying using Serverless on GoogleCloud and AWS

## Installing required plugins

```bash
sls plugin install -n serverless-wsgi
sls plugin install -n serverless-python-requirements
```

## Local development

```bash
sls wsgi serve
```

## Deployment on AWS

```bash
sls deploy -v
```

## See logs

```bash
sls logs -f app
```

## Endpoints

`/` - say hi
`/quote` - get random smart quote